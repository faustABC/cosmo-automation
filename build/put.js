"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var test_json_1 = __importDefault(require("./test.json"));
var node_fetch_1 = __importDefault(require("node-fetch"));
test_json_1.default.forEach(function (item) { return __awaiter(void 0, void 0, void 0, function () {
    var res, _a, error, value, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                _b.trys.push([0, 3, , 4]);
                return [4 /*yield*/, node_fetch_1.default("http://localhost:5050/data/data-store/http%3A%2F%2Fplatform.cosmoconsult.com%2Fontology%2FCpaPage/" + encodeURIComponent(item.identifier), {
                        method: "put",
                        body: JSON.stringify(item),
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlcxSkd0TWVEbmdjN1d5cFZlV0hfckItamViZG5OTng5S09jaTU2Qlp3MjAifQ.eyJpc3MiOiJodHRwczovL2Nvc21vcGxhdGZvcm0uYjJjbG9naW4uY29tL2MxY2FkNTg0LTA4MzQtNGY5NS05ZjVlLTcyZjQxYTBjMTVkMC92Mi4wLyIsImV4cCI6MTYwMDkxMDY4NSwibmJmIjoxNjAwODI0Mjg1LCJhdWQiOiIyNDcwNjFlNS0xOWJjLTRiMmQtOWY3Ni04Nzk0N2Y2ZTc2NTUiLCJnaXZlbl9uYW1lIjoiQWxleCIsImZhbWlseV9uYW1lIjoiR3VzZXYiLCJuYW1lIjoiQWxleCBHdXNldiIsImlkcCI6Imdvb2dsZS5jb20iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhbGV4c2VyZ2VldmljaGd1c2V2QGdtYWlsLmNvbSIsInN1YiI6ImE5M2I1NDM4LTBhNGUtNDJkMi04Y2U5LWExMzM5MjJkOWZhNSIsIm90aGVyX2VtYWlscyI6WyJhZ3VzZXZAZWxpbmV4dC5jb20iLCJhbGV4c2VyZ2VldmljaGd1c2V2QGdtYWlsLmNvbSJdLCJlbWFpbCI6ImFndXNldkBlbGluZXh0LmNvbSIsImNwX2dhbiI6InRiZCIsImNwX2FuIjpbIntcImNvbXBhbnlcIjpcIkNPU01PIENPTlNVTFQgR21iSCBCZXJsaW5cIixcImFjY291bnRzXCI6W3tcInR5cGVcIjpcIkNvbnRhY3RcIixcIm5vXCI6XCJLTlQxMDAwNjk0XCJ9LHtcInR5cGVcIjpcIkN1c3RvbWVyXCIsXCJub1wiOlwiMTk5MTAwXCIsXCJyZWxDb250Tm9cIjpcIktUMjAxMzc2LjFcIn0se1widHlwZVwiOlwiQ29udGFjdFwiLFwibm9cIjpcIktUMjAxMzc2LjFcIn1dfSJdLCJjcF9hbl9pdGVtX3R5cGUiOiJlc2NhcGVkSnNvbiIsIm5vbmNlIjoiMDkwZTBhOTUtOGU0Ni00ZmZhLWI2ODQtZGI3Mjg5OTdjNzY0Iiwic2NwIjoib2F1dGguY3BiLnJlYWQiLCJhenAiOiIwY2U0ODQyYy1kMzJjLTRiMDYtYWIxOS05MjZhYWQ3MDUxODYiLCJ2ZXIiOiIxLjAiLCJpYXQiOjE2MDA4MjQyODV9.QwQisAgzoLlpHxFD5Ir2JcAlt_9Xmhf6Suze5OGrKrLfJOt3u1hjtj6qm1t7WCzx1Im1azfKLNtBqYTB5SmlSUBCvfG4JDi1tTxMJZBovm-QZTDskCBKxqS9GZ76Al25VvqB_SYW-QLdpV9J6jhFXddlrpOdXNdk9U7XTBGuKC1lIBObA0kgRKNPPWcY_Ks30twSPCh542Aq-ptdfIieXBOjb5MVH8zjIlGggthnOH-HlobykKYr46cruH7I4sktkPL-VoxHtVj7JpHam9mf_1e6LINUdFj4YdnQhCpwNwCzAyPKMO_MuHy6xLikrQ0PqFHlnmRTQb6GZS8Czwgz7A",
                        },
                    })];
            case 1:
                res = _b.sent();
                return [4 /*yield*/, res.json()];
            case 2:
                _a = _b.sent(), error = _a.error, value = _a.value;
                console.log(error !== null && error !== void 0 ? error : { success: value.identifier });
                return [3 /*break*/, 4];
            case 3:
                e_1 = _b.sent();
                console.log(item.identifier + " : " + e_1.message);
                return [3 /*break*/, 4];
            case 4: return [2 /*return*/];
        }
    });
}); });
