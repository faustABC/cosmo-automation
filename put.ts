import data from "./test.json";
import fetch from "node-fetch";

data.forEach(async (item) => {
  try {
    const res = await fetch(
      `http://localhost:5050/data/data-store/http%3A%2F%2Fplatform.cosmoconsult.com%2Fontology%2FCpaPage/${encodeURIComponent(
        item.identifier
      )}`,
      {
        method: process.argv[2] as "POST" | "PUT",
        body: JSON.stringify(item),
        headers: {
          "Content-Type": "application/json",
          Authorization:
            "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6IlcxSkd0TWVEbmdjN1d5cFZlV0hfckItamViZG5OTng5S09jaTU2Qlp3MjAifQ.eyJpc3MiOiJodHRwczovL2Nvc21vcGxhdGZvcm0uYjJjbG9naW4uY29tL2MxY2FkNTg0LTA4MzQtNGY5NS05ZjVlLTcyZjQxYTBjMTVkMC92Mi4wLyIsImV4cCI6MTYwMDkxMDY4NSwibmJmIjoxNjAwODI0Mjg1LCJhdWQiOiIyNDcwNjFlNS0xOWJjLTRiMmQtOWY3Ni04Nzk0N2Y2ZTc2NTUiLCJnaXZlbl9uYW1lIjoiQWxleCIsImZhbWlseV9uYW1lIjoiR3VzZXYiLCJuYW1lIjoiQWxleCBHdXNldiIsImlkcCI6Imdvb2dsZS5jb20iLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJhbGV4c2VyZ2VldmljaGd1c2V2QGdtYWlsLmNvbSIsInN1YiI6ImE5M2I1NDM4LTBhNGUtNDJkMi04Y2U5LWExMzM5MjJkOWZhNSIsIm90aGVyX2VtYWlscyI6WyJhZ3VzZXZAZWxpbmV4dC5jb20iLCJhbGV4c2VyZ2VldmljaGd1c2V2QGdtYWlsLmNvbSJdLCJlbWFpbCI6ImFndXNldkBlbGluZXh0LmNvbSIsImNwX2dhbiI6InRiZCIsImNwX2FuIjpbIntcImNvbXBhbnlcIjpcIkNPU01PIENPTlNVTFQgR21iSCBCZXJsaW5cIixcImFjY291bnRzXCI6W3tcInR5cGVcIjpcIkNvbnRhY3RcIixcIm5vXCI6XCJLTlQxMDAwNjk0XCJ9LHtcInR5cGVcIjpcIkN1c3RvbWVyXCIsXCJub1wiOlwiMTk5MTAwXCIsXCJyZWxDb250Tm9cIjpcIktUMjAxMzc2LjFcIn0se1widHlwZVwiOlwiQ29udGFjdFwiLFwibm9cIjpcIktUMjAxMzc2LjFcIn1dfSJdLCJjcF9hbl9pdGVtX3R5cGUiOiJlc2NhcGVkSnNvbiIsIm5vbmNlIjoiMDkwZTBhOTUtOGU0Ni00ZmZhLWI2ODQtZGI3Mjg5OTdjNzY0Iiwic2NwIjoib2F1dGguY3BiLnJlYWQiLCJhenAiOiIwY2U0ODQyYy1kMzJjLTRiMDYtYWIxOS05MjZhYWQ3MDUxODYiLCJ2ZXIiOiIxLjAiLCJpYXQiOjE2MDA4MjQyODV9.QwQisAgzoLlpHxFD5Ir2JcAlt_9Xmhf6Suze5OGrKrLfJOt3u1hjtj6qm1t7WCzx1Im1azfKLNtBqYTB5SmlSUBCvfG4JDi1tTxMJZBovm-QZTDskCBKxqS9GZ76Al25VvqB_SYW-QLdpV9J6jhFXddlrpOdXNdk9U7XTBGuKC1lIBObA0kgRKNPPWcY_Ks30twSPCh542Aq-ptdfIieXBOjb5MVH8zjIlGggthnOH-HlobykKYr46cruH7I4sktkPL-VoxHtVj7JpHam9mf_1e6LINUdFj4YdnQhCpwNwCzAyPKMO_MuHy6xLikrQ0PqFHlnmRTQb6GZS8Czwgz7A",
        },
      }
    );
    const { error, value } = await res.json();

    console.log(error ?? { success: value.identifier });
  } catch (e) {
    console.log(item.identifier + " : " + e.message);
  }
});
